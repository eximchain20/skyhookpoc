pragma solidity ^0.4.19;
contract Satellite {
  function getLocation(address _clientContract) public view returns (uint, uint);
}

contract BetterOracle {
  //define vars/structs we will ask from oracle provider's satelite
  uint public longitute;
  uint public latitude;

  //define address for owner (client and gpsOracle)
  address public gpsOracle;
  address public client;

  //define satelite address  managed by oracle provider and approved by client
  address satelliteAddress;
  address suggestedSatelliteAddress;

  //define state for updating oracle provider's satelite
  enum State {ModificationSuggested,ModificationApproved}
  State public state;

  // Define modifiers
  // Do not forget the "_;"! It will be replaced by the actual function body when the modifier is used.
  modifier isOracle{
    require(msg.sender == gpsOracle);
    _;
  }
  modifier isClient{
    require(msg.sender == client);
    _;
  }
  modifier inState(State _state) {
    require(state == _state);
    _;
  }

  //define functions to set and update satelite address by client (owner)
  function BetterOracle() public{
    client = msg.sender;
  }
  function setSatellite(address _gpsOracle, address _satelliteAddress) public isClient {
        gpsOracle = _gpsOracle;
        satelliteAddress = _satelliteAddress;
  }
  //define functions to set and update satelite address by oracle gps satellite provider
  function suggestModification(address _suggestedSatelliteAddress) public isOracle{
    suggestedSatelliteAddress = _suggestedSatelliteAddress;
    state = State.ModificationSuggested;
  }
  function approveModification() inState(State.ModificationSuggested) public isClient{
    satelliteAddress = suggestedSatelliteAddress;
    state = State.ModificationApproved;
  }

  //call satelite oracle and update the GPS location
  function updateLocation() public{
      //intantiate contract
      Satellite m = Satellite(satelliteAddress);
      // create a few variables in memory to grab returns from GPS Satellite
      uint _long;
      uint _lat;
      //call getlocation on Satellite(satelliteAddress) for this contract and place in memory
      (_long,_lat) = m.getLocation(this);
      //update internal contract storage
      longitute = _long;
      latitude = _lat;
  }

}
