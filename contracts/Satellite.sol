pragma solidity ^0.4.19;
contract Satellite {
  //define vars/structs

  struct Location {
       uint longitude;
       uint latitude;
   }
  address public owner;
  mapping  (address => Location) clientLocations;

   function Satellite() public{
     owner = msg.sender;
   }

   // allows execution by the owner only
  modifier ownerOnly {
      assert(msg.sender == owner);
      _;
  }
  function newSubscription(address _clientContract, uint _long, uint _lat) public ownerOnly{
    // Creates new struct in memory.
    Location memory location = Location(_long, _lat);
    //add created object to mapping
    clientLocations[_clientContract] = location;
  }

  function getLocation(address _clientContract) public view returns (uint, uint) {
    return (clientLocations[_clientContract].longitude,
            clientLocations[_clientContract].latitude);
   }

  function setLocation(address _clientContract, uint _long, uint _lat) public ownerOnly{
    Location client = clientLocations[_clientContract];
    client.longitude = _long;
    client.latitude = _lat;
  }
}
